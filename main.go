package main

import (
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"fmt"

	"gopkg.in/gin-gonic/gin.v1"
)

func main() {
	router := gin.Default()

	router.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "pong",
		})
	})

	router.PUT("/ical", func(c *gin.Context) {
		file, _, err := c.Request.FormFile("file")

		out, err := ioutil.TempFile("/tmp/", "ical2json_")
		if err != nil {
			log.Fatal(err)
		}
		defer out.Close() // FIXME os.Remove(out.Name()) // clean up
		fmt.Printf("filename = %s", out.Name)

		bytesWritten, err := io.Copy(out, file)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("ical file, bytes written: %d", bytesWritten)

		/*
			_, err = ical.ParseCalendar(file.Read)
			if err != nil {
				log.Fatal(err)
			}
		*/
		if err := out.Close(); err != nil {
			log.Fatal(err)
		}

		c.JSON(201, gin.H{
			"status": "created",
		})
	})

	router.Run() // listen and serve on 0.0.0.0:8080
}
